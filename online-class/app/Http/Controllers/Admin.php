<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Encryption\DecryptException;

use App\M_Admin;

class Admin extends Controller
{
    // tambah admin
    public function tambahAdmin(Request $request){
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'email' => 'required | unique:tbl_user',
            'password' => 'required',
            'token' => 'required'

        ]); // semua inputan yang akan divalidasi

        // validasi inputan dari form, cek pakai postman
        // cek ada atau tidak
        if($validator->fails()){
            return response()->json([ // kembalikan nilai pakai response dalam bentuk json
                'status' => 'gagal', 
                'message' => $validator->messages()
            ]);
        }

        if(M_Admin::create([
                            'nama' => $request->nama,
                            'email' => $request->email,
                            'password' => encrypt($request->password)
                        ])){
                        return response()->json([
                            'status' => 'berhasil',
                            'message' => 'Data berhasil disimpan'
                        ]);
                        }else{
                                return response()->json([
                                'status' => 'gagal',
                                'message' => 'Data gagal disimpan'
                            ]);
                        }    

        $token = $request->token; //ambil token
        $tokenDb = M_Admin::where('token', $token)->count(); //cek tokennya ada atau tidak
            if($tokenDb > 0){
                $key = env('APP_KEY');
                $decoded = JWT::decode($token, $key, array('HS256')); 
                $decoded_array = (array) $decoded; // membaca isi dari decode

                if($decoded_array['extime'] > time()){ // validasi token kadaluwarsa atau belum
                        if(M_Admin::create([ // jikauntuk  model admin ditambahkan data, maka akan ditampilkan repon nya apa
                            'nama' => $request->nama,
                            'email' => $request->email,
                            'password' => encrypt($request->password)
                        ])){
                        return response()->json([
                            'status' => 'berhasil',
                            'message' => 'Data berhasil disimpan'
                        ]);
                        }else{
                                return response()->json([
                                'status' => 'gagal',
                                'message' => 'Data gagal disimpan'
                            ]);
                        }                   
                }else{
                    return response()->json([
                    'status' => 'gagal',
                    'message' => 'Token kadaluwarsa'
                ]);
                } 
           }   
    }

     // login admin
    public function loginAdmin(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required'
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'gagal', 
                'message' => $validator->messages()
            ]);
        }
        $cek = M_Admin::where('email', $request->email)->count();
        $admin = M_Admin::where('email', $request->email)->get();

        if($cek > 0){
            foreach ($admin as $adm) {
                if($request->password == decrypt($adm->password)){
                    $key = env('APP_KEY');
                    $data = array(
                        "extime" => time()+(60*120), // time dibuat 60 second * 120, masa kadaluarsa
                        "id_admin" => $adm->id_user
                   );
                   $jwt = JWT::encode($data, $key, 'HS256');
                   // update token
                   M_Admin::where('id_user', $adm->id_user)->update(
                        [
                            'token' => $jwt
                        ]
                    );
                   return response()->json([
                        'status' => 'berhasil',
                        'message' => 'Berhasil login',
                        'token' => $jwt
                    ]);

                }else{
                    return response()->json([
                        'status' => 'gagal',
                        'message' => 'Password Salah'

                    ]);
                }
            }

        }else{
            return response()->json([
                'status' => 'gagal',
                'message' => 'Email tidak terdaftar'
            ]);
        }
    }

    // hapus admin
    public function hapusAdmin(Request $request){
        $validator = Validator::make($request->all(), [
            'id_user' => 'required',            
            'token' => 'required'

        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 'gagal', 
                'message' => $validator->messages()
            ]);
        }

            $token = $request->token;
            $tokenDb = M_Admin::where('token', $token)->count();
            if($tokenDb > 0){
                $key = env('APP_KEY');
                // $decoded = JWT::decode($token, $key, 'HS256');
                $decoded = JWT::decode($token, new Key($key, 'HS256'));
                // $decoded = JWT::decode($token, $key, array('HS256')); // array assosiative

                $decoded_array = (array) $decoded;

                if($decoded_array['extime'] > time()){
                        if(M_Admin::where('id_user', $request->id_user)->delete()){
                        return response()->json([
                            'status' => 'berhasil',
                            'message' => 'Data berhasil dihapus'
                        ]);
                        }else{
                                return response()->json([
                                'status' => 'gagal',
                                'message' => 'Data gagal dihapus'
                            ]);
                        }                   
                }else{
                    return response()->json([
                    'status' => 'gagal',
                    'message' => 'Token kadaluwarsa'
                ]);
                } 
            }       
    }
}
